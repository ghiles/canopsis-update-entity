
#!/usr/bin/env python
# -*- coding:Utf-8 -*-
# Author Ghiles KHEDDACHE gkheddache@somone.fr

import requests
from requests.auth import HTTPBasicAuth
import json
import os
from MySQLdb import _mysql
from csv import reader
import json
if os.path.exists("/tmp/ACL-H24.csv"):
  os.remove("/tmp/ACL-H24.csv")

db=_mysql.connect("localhost","root","xxxxx", "centreon_ref")
db.query("""SELECT * from host limit 1 """)
r=db.store_result()
r.fetch_row()
db.query("""select s.service_id,h.host_id, s.service_description, h.host_name, hg.hg_id, hg.hg_name, group_concat(res.acl_res_name SEPARATOR ',') as acls_list from host h, hostgroup hg, hostgroup_relation hgr, service s, host_service_relation hsr, acl_resources_hg_relations res_r_hg, acl_resources res where  h.host_id=hsr.host_host_id and s.service_id=hsr.service_service_id and h.host_id=hgr.host_host_id and hg.hg_id=hgr.hostgroup_hg_id and hg.hg_id=res_r_hg.hg_hg_id and res_r_hg.acl_res_id=res.acl_res_id group by s.service_id,h.host_id, s.service_description, h.host_name, hg.hg_id, hg.hg_name INTO outfile '/tmp/ACL-H24.csv' FIELDS TERMINATED BY ';' LINES TERMINATED BY '\r\n' """)
myjson = {}
hg = {}
acl = {}
infos = {}
entity = {}
lnk = []
cis = []
url = 'http://localhost/api/contextgraph/import'
headers = {'Content-type': 'application/x-www-form-urlencoded'}

with open('/tmp/ACL-H24.csv', 'r') as read_obj:
        csv_reader = reader(read_obj,delimiter=';')
        for row in csv_reader:
                hostgroup=row[5]
                host=row[3]
                service=row[2]
                acl_list=row[6].split(',')
                #print (acl)

                hg["name"] = "HG"
                hg["value"] = hostgroup
                hg["description"] = "hostgroup"

                acl["name"] = "ACL"
                acl["value"] = acl_list
                acl["description"] = "ACL"
                acl["description"] = "ACL"

                infos["hg"] = hg
                infos["acl"] = acl

                entity["name"] = service
                entity["enabled"] = "true"
                entity["action"] = "set"
                entity["infos"] = infos
                entity["_id"] = service + '/' + host
                entity["type"] = "component"

                cis = [entity]
                myjson["cis"] = cis
                myjson["links"] = lnk
                #myjson01 = dict({"cis": [{"name": host, "enabled": "true", "action": "set", "infos": {"HG":{"name": "HG","value": hg, "description": "hostgroups"}, "ACL": { "name": "ACL", "value": acl, "description": "ACL"}}, "_id": "test","type": "component"}],"links": []})
                #print(json.dumps(myjson))

                json_data = json.dumps(myjson)
                payload = 'json=' + json_data
                #print(json_data)
                #auth=HTTPBasicAuth('root', 'xxxxx')
#               r = requests.put(url, data=payload, auth=HTTPBasicAuth('root', 'xxxxx'), headers=headers)
#               print(r.text)
                #curl -X PUT -u root:xxxxx -H "Content-type: application/x-www-form-urlencoded" -d json=  'http://localhost/api/contextgraph/import'
